class Pi {//Path item
    constructor(text, event, children, opts) {
        this.text = text;
        this.event = event;
        if (children != null) {
            this.children = [...children];
        }
        this.optionsList = [];
        if (opts != null) {
            this.addOpts(opts);
        }
    }

    equals(other) {
        let comp = this.text === other.text && this.event === other.event;
        return comp;
    }

    static event(text, children, opts) {
        return new Pi(text, true, children, opts);
    }

    static action(text, children, opts) {
        return new Pi(text, false, children, opts);
    }

    addOpts(optsToAdd) {
        this.optionsList = this.optionsList.concat(optsToAdd);
        return this;
    }

    getOpts() {
        return this.optionsList;
    }

    possiblePaths() {
        return Pi.possiblePaths([], this, []);
    }

    useCpy(children) {
        return new Pi(this.text, this.event, [].concat(children), this.opts);
    }

    static possiblePaths(path, current, result) {
        path.push(current);
        if (current.children == null) {
            result.push(Question.fromPathTree(path));
            return result;
        } else {
            for (var i = 0; i < current.children.length; i++) {
                Pi.possiblePaths([...path], current.children[i], result);
            }
            return result;
        }
    }
}

class Question {
    constructor(path, options) {
        this.path = path;
        this.options = options;
    }

    static qAct(path, option) {
        return new Question(path, option);
    }

    static oneQ(question, answer, options) {
        let ans = Pi.action(answer);
        return new Question([Pi.event(question), ans], options.map(o => Pi.action(o)).concat(ans));
    }

    static order(question, order, extraOps) {
        let orderOpt = order.map(i => Pi.action(i));
        if (extraOps == null) {
            extraOps = [];
        }
        return new Question([Pi.event(question)].concat(orderOpt), orderOpt.concat(extraOps.map(i => Pi.action(i))));
    }

    static qManyA(question, answers, options) {
        let ans = answers.map(a => Pi.action(a));
        return new Question([Pi.event(question), ans], options.map(o => Pi.action(o)).concat(ans));
    }

    static fromPathTree(path) {
        let optionsFromPath = [];
        path.forEach(pt => pt.getOpts().forEach(o => optionsFromPath.push(o)));
        path.filter(pt => !pt.event).forEach(pt => optionsFromPath.push(pt));
        optionsFromPath = Questionary.onlyUnique(optionsFromPath);

        return Question.qAct(path, optionsFromPath);
    }
}

class Questionary {
    constructor(target, questions, endText) {
        this.target = target;
        this.questions = questions;
        this.finished = [];
        this.wrongGuesses = 0;
        this.startNew();
        this.partialCorrect = [];

        this.endText = endText;
        if (this.endText == null) {
            this.endText = wrongs => `<p>Well done you finished the quiz!</p><p>You made a total of ${wrongs} wrong guesses!</p>`;
        }
    }

    setCongratulationText(congrats, performance) {
        this.congrats = congrats;
        this.performance = performance;
    }

    guessFunction(text) {
        let next = this.current.path[this.progress + 1];
        let right = [].concat(next);
        let match = right.filter(t => t.text === text);

        if (match.length == 1) {
            this.partialCorrect.push(match[0]);
            if (this.partialCorrect.length == right.length) {
                this.progressFunction();
            }
            return true;
        } else {
            this.wrongGuesses++;
            return false;
        }
    }

    progressFunction() {
        this.progress++;
        this.partialCorrect = [];
        while (this.current.path.length > (this.progress + 1) && this.current.path[this.progress + 1].event) {
            this.progress++;
        }
        this.draw(this.progress + 1 == this.current.path.length);
    }

    next() {
        if (!this.madeMistake) {
            this.finished.push(this.current);
        }
        this.startNew();
    }

    startNew() {
        this.progress = 0;
        this.madeMistake = false;
        let possible = this.questions.filter(p => !this.finished.some(pt => pt == p));
        if (possible.length > 0) {
            this.current = possible[Math.floor(Math.random() * possible.length)];
            Questionary.shuffleArray(this.current.options);
            this.draw(false);
        } else {
            this.drawFinished();
        }
    }

    draw(finished) {
        $('#' + this.target).html('');
        var html = `<progress value=${this.finished.length} max=${this.questions.length} class="progressss"></progress>`;
        html += '<div class="questionnaire">';
        for (let i = 0; i < this.progress + 1; i++) {
            let classes = 'right guess';
            if (this.current.path[i].event) {
                classes = '';
            }
            [].concat(this.current.path[i]).forEach(t => html += `<div class="${classes}"><span>${t.text}</span></div>`);
            if (i < this.current.path.length - 1 || !finished) {
                html += '<span class="arrow">↓</span>';
            }
        }
        html += '</div>';
        if (!finished) {
            html += '<div class="questionnaire">\n';
            let used = this.current.path.slice(0, this.progress + 1);
            let options = this.current.options.filter(p => !used.some(pt => pt.equals(p)));
            options = options.concat(this.current.path.slice(this.progress + 1).flat().filter(pi => !pi.event));
            options = Questionary.onlyUnique2(options);
            Questionary.shuffleArray(options);
            options.forEach(t => html += `<div class="unguessed guess">${t.text}</div>\n`);
            html += '</div>';
        } else {
            html += '<button class="nextButton btn btn-primary ">Next</button>'
        }
        $('#' + this.target).html(html);
        $('#' + this.target + ' .nextButton').bind('click', { quest: this }, (e) => e.data.quest.next());
        $('#' + this.target + ' .unguessed').bind('click', { quest: this }, function (e) {
            $(this).unbind(e);
            let guess = $(this).html();
            let correct = e.data.quest.guessFunction(guess);
            if (!correct) {
                $(this).removeClass("unguessed");
                $(this).addClass("wrong");
                let correct = e.data.quest.madeMistake = true;
            } else {
                $(this).removeClass("unguessed");
                $(this).addClass("right");
            }
        });
    }

    drawFinished() {
        $('#' + this.target).html('');
        var html = `<progress value=${this.finished.length} max=${this.questions.length} class="progressss"></progress>`;
        html += '<div class="questionnaire">';
        html += this.endText(this.wrongGuesses);
        html += '<button class="nextButton btn btn-info">Go again!</button>';
        html += '</div>';
        $('#' + this.target).html(html);
        $('#' + this.target + ' .nextButton').bind('click', { quest: this }, (e) => {
            e.data.quest.finished = [];
            e.data.quest.wrongGuesses = 0;
            e.data.quest.startNew();
        });
    }

    static shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    static onlyUnique(array) {
        return array.filter((value, index, self) =>
            index === self.findIndex((t) => t.equals(value))
        );
    }

    static onlyUnique2(array) {
        let map = new Map();

        array.forEach(pi => map.set(pi.text, pi));

        return Array.from(map.values());
    }
}