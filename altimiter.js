var drawer;
var done = true;
var drawPause = 16;
var fallspeed = 50;
var mheight = 4000.0;

var lastDraw = 0;

var going = false;;

var currHeight;

$(document).ready(function() {
    $('#playbutton').click(function() {
        if (!going) {
            start();
        } else {
            stop();
        }
    });
    $('#resetbutton').click(reset);
    $('#exitaltitude').change(reset);

    reset();
});

function start() {
    lastDraw = new Date().getTime();
    drawer = setInterval(redraw, drawPause);
    $("#playbutton").html('Stop');
    if(currHeight == 0){
    	currHeight = $("#exitaltitude").val();
    }
    going = true;
}

function stop() {
    clearInterval(drawer);
    $("#playbutton").html('Jump');
    going = false;
}

function reset() {
    stop();
    currHeight = $("#exitaltitude").val();
    step(0);
}

function redraw() {
    if (!done) {
        console.log("skipping");
        return;
    }
    var interval = new Date().getTime() - lastDraw;
    step(interval);
    lastDraw = new Date().getTime();

}

function step(interval) {
    done = false;
    currHeight -= fallspeed / 1000 * interval;
    if (currHeight <= 0) {
        currHeight = 0;
        stop();
    }
    drawAnalog(currHeight);
    done = true;
}

function drawAnalog(height) {
	//console.log("drawing: " + height);
    var analogCanv = document.getElementById('analog');
    analogCanv.width = 400;
    analogCanv.height = 400;
    var analogCtx = analogCanv.getContext("2d");

    var lineLength = 120;

    var lineAngle = height / 4000 * 2 * Math.PI + Math.PI;
    var centerX = 203;
    var centerY = 199;

    analogCtx.lineWidth = 7;
    analogCtx.beginPath();
    analogCtx.lineCap = "round";
    analogCtx.moveTo(centerX, centerY);
    analogCtx.lineTo(centerX - Math.sin(lineAngle) * lineLength, centerY + Math.cos(lineAngle) * lineLength);
    analogCtx.stroke()
}