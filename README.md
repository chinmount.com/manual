# Fallskärmsmanual

En tänkt manual att ha som hjälpmedel till fallskärmsutbildning i Sverige.

## Usage
Freely available at https://manual.chinmount.com


## Contributing
Gladly accept improvements and localizations.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
Free to access on the page above. Free to contribute localizations. Don't fork. Don't use with complementary material that would be better suited included here.

## Project status
Early in its life.
